package api

import (
	"github.com/dgrijalva/jwt-go"
	"gophersumit.com/sa-users/pkg/models"
)

// REST provides dependencies required by REST API Handlers
type REST struct {
	BaseURL string
	Emailer interface {
		SendEmail(*models.Email) error
	}

	Users interface {
		Insert(name, email, password string) error
		Authenticate(email, password string) (int, error)
		Get(int) (*models.User, error)
		ChangePassword(email string, newPassword string) error
		GetHash(email string) (*models.TokenDetails, error)
		CheckEmail(email string) (int, error)
		CreateGoogleUser(ge *GoogleEmail) (int, error)
	}

	UserDetails interface {
		Get(id int) (*models.UserDetails, error)
		Update(id int, userDetails *models.UserDetails) error
		Create(userDetails *models.UserDetails) error
	}

	TokenService interface {
		CreateToken(userid int, hash string) (string, error)
		VerifyToken(tokenString string, hash string) (*jwt.Token, error)
		ExtractUserID(token *jwt.Token) (int, error)
	}
}
