package api

import (
	"net/http"

	"gophersumit.com/sa-users/pkg/models"
)

// RegisterUser handles routing related to users
func (rest *REST) RegisterUser(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		rest.userRegister(w, r)
		return

	default:
		postError(w, http.StatusMethodNotAllowed)
	}
}

// ChangePassword user password
func (rest *REST) ChangePassword(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		rest.changePassword(w, r)
		return

	default:
		postError(w, http.StatusMethodNotAllowed)
	}
}

func (rest *REST) userRegister(w http.ResponseWriter, r *http.Request) {
	ru := new(RegisterUser)
	err := bodyToRegister(r, ru)

	if err != nil {
		postError(w, http.StatusBadRequest)
		return
	}

	err = rest.Users.Insert(ru.Name, ru.Email, ru.Password)

	if err != nil {
		if err.Error() == models.ErrDuplicateEmail.Error() {
			postBodyResponse(w, http.StatusForbidden, jsonResponse{"error": "email address is already registered"})
			return
		}
		postError(w, http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func (rest *REST) changePassword(w http.ResponseWriter, r *http.Request) {
	cp := &ChangePasswordRequest{}
	err := bodyToChangePassword(r, cp)

	if err != nil {
		postError(w, http.StatusBadRequest)
		return
	}

	err = rest.Users.ChangePassword(cp.Email, cp.NewPassword)

	if err != nil {
		postError(w, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	return

}
