package api

import (
	"errors"
	"net/http"
	"strconv"
	"strings"

	"gophersumit.com/sa-users/pkg/models"
)

// UserProfile user profile related api endpoints
func (rest *REST) UserProfile(w http.ResponseWriter, r *http.Request) {
	path := strings.TrimSuffix(r.URL.Path, "/")
	if path == "/api/user/profile" {
		switch r.Method {
		case http.MethodPost:
			rest.createUserProfile(w, r)
			return
		default:
			postError(w, http.StatusMethodNotAllowed)
			return
		}
	}

	path = strings.TrimPrefix(path, "/api/user/profile/")
	id, err := strconv.Atoi(path)
	if err != nil {
		postError(w, http.StatusBadRequest)
		return
	}

	if id <= 0 {
		postError(w, http.StatusBadRequest)
		return
	}

	switch r.Method {
	case http.MethodGet:
		rest.getUserProfile(w, r, id)
		return
	case http.MethodPut:
		rest.updateUserProfile(w, r, id)
		return
	default:
		postError(w, http.StatusMethodNotAllowed)
		return

	}

}

func (rest *REST) getUserProfile(w http.ResponseWriter, r *http.Request, id int) {
	ud, err := rest.UserDetails.Get(id)

	if err != nil {
		postError(w, http.StatusInternalServerError)
		return
	}

	postBodyResponse(w, http.StatusOK, jsonResponse{"userDetails": ud})
}

func (rest *REST) updateUserProfile(w http.ResponseWriter, r *http.Request, id int) {
	ud := new(models.UserDetails)
	err := bodyToUserDetails(r, ud)

	if err != nil {
		postError(w, http.StatusBadRequest)
		return
	}

	err = rest.UserDetails.Update(id, ud)

	if err != nil {
		if errors.Is(err, models.ErrDuplicateEmail) {
			postError(w, http.StatusConflict)
			return
		}
		postError(w, http.StatusUnauthorized)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (rest *REST) createUserProfile(w http.ResponseWriter, r *http.Request) {
	ud := new(models.UserDetails)
	err := bodyToUserDetails(r, ud)
	if err != nil {
		postError(w, http.StatusBadRequest)
		return
	}

	err = rest.UserDetails.Create(ud)
	if err != nil {
		postError(w, http.StatusUnauthorized)
		return
	}

	w.WriteHeader(http.StatusCreated)
}
