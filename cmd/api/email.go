package api

import (
	"bytes"
	"html/template"
	"net/http"

	"gophersumit.com/sa-users/pkg/models"
)

// EmailContent is email body
type EmailContent struct {
	ResetLink string
}

// Send email
func (rest *REST) Send(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		rest.send(w, r)
		return

	default:
		postError(w, http.StatusMethodNotAllowed)
	}
}

func (rest *REST) send(w http.ResponseWriter, r *http.Request) {

	e := new(Email)
	err := bodyToEmail(r, e)

	if err != nil {
		postError(w, http.StatusBadRequest)
		return
	}
	link := rest.BaseURL + "/user/changepassword?token=" + e.ResetLink + "&email=" + e.ToEmail

	ebody := &EmailContent{
		ResetLink: link,
	}
	res, err := template.ParseFiles("./templates/email/email.html")

	if err != nil {
		postError(w, http.StatusInternalServerError)
	}
	var tmplBytes bytes.Buffer
	err = res.Execute(&tmplBytes, ebody)
	if err != nil {
		postError(w, http.StatusInternalServerError)
	}
	email := &models.Email{
		Subject: e.Subject,
		Body:    tmplBytes.String(),
		ToEmail: e.ToEmail,
	}
	err = rest.Emailer.SendEmail(email)
	if err != nil {
		postError(w, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}
