package api

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"gophersumit.com/sa-users/pkg/models"
)

func bodyToEmail(r *http.Request, e *Email) error {
	err := checkRequest(r)
	if err != nil {
		return err
	}
	if e == nil {
		return errors.New("email struct is required")
	}
	bd, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	return json.Unmarshal(bd, e)
}

func bodyToLogin(r *http.Request, l *LoginUser) error {
	err := checkRequest(r)
	if err != nil {
		return err
	}

	if l == nil {
		return errors.New("login struct is required")
	}
	bd, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	return json.Unmarshal(bd, l)
}

func bodyToRegister(r *http.Request, ru *RegisterUser) error {
	err := checkRequest(r)

	if err != nil {
		return err
	}

	if ru == nil {
		return errors.New("register struct is required")
	}

	bd, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	return json.Unmarshal(bd, ru)
}

func postError(w http.ResponseWriter, code int) {
	http.Error(w, http.StatusText(code), code)
}

func checkRequest(r *http.Request) error {
	if r == nil {
		return errors.New("a request is required")
	}
	if r.Body == nil {
		return errors.New("request body is empty")
	}

	return nil
}

func postBodyResponse(w http.ResponseWriter, code int, content jsonResponse) {
	if content != nil {
		js, err := json.Marshal(content)
		if err != nil {
			postError(w, http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(code)
		w.Write(js)
		return
	}
	w.WriteHeader(code)
	w.Write([]byte(http.StatusText(code)))
}

func bodyToUserDetails(r *http.Request, ud *models.UserDetails) error {
	err := checkRequest(r)
	if err != nil {
		return err
	}

	if ud == nil {
		return errors.New("userDetails struct is required")
	}

	bd, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	return json.Unmarshal(bd, ud)
}

func bodyToUserID(r *http.Request, userID *UserEmail) error {
	err := checkRequest(r)

	if err != nil {
		return err
	}

	bd, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	return json.Unmarshal(bd, userID)
}

func bodyToChangePassword(r *http.Request, cp *ChangePasswordRequest) error {
	err := checkRequest(r)

	if err != nil {
		return err
	}

	bd, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	return json.Unmarshal(bd, cp)
}

func bodyToTokenData(r *http.Request, td *TokenData) error {
	err := checkRequest(r)

	if err != nil {
		return err
	}

	bd, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	return json.Unmarshal(bd, td)
}

func bodyToGoogleEmail(w http.ResponseWriter, r *http.Request, ge *GoogleEmail) error {
	err := checkRequest(r)

	if err != nil {
		return err
	}

	bd, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	return json.Unmarshal(bd, ge)
}
