package api

import (
	"errors"
	"net/http"

	"gophersumit.com/sa-users/pkg/models"
)

// Token related api for password reset
func (rest *REST) Token(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		rest.generateToken(w, r)
		return

	default:
		postError(w, http.StatusMethodNotAllowed)
	}
}

// ValidateToken that token is valid
func (rest *REST) ValidateToken(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		rest.validateToken(w, r)
		return

	default:
		postError(w, http.StatusMethodNotAllowed)
	}
}

func (rest *REST) generateToken(w http.ResponseWriter, r *http.Request) {
	u := new(UserEmail)
	err := bodyToUserID(r, u)

	if err != nil {
		postError(w, http.StatusBadRequest)
		return
	}

	td, err := rest.Users.GetHash(u.Email)
	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			postError(w, http.StatusForbidden)
			return
		}

		postError(w, http.StatusInternalServerError)
		return

	}

	t, err := rest.TokenService.CreateToken(td.UserID, td.Hash)
	if err != nil {
		postError(w, http.StatusInternalServerError)
		return
	}

	postBodyResponse(w, http.StatusOK, jsonResponse{"token": t})
}

func (rest *REST) validateToken(w http.ResponseWriter, r *http.Request) {
	td := new(TokenData)
	err := bodyToTokenData(r, td)

	if err != nil {
		postError(w, http.StatusBadRequest)
		return
	}
	details, err := rest.Users.GetHash(td.Email)
	if err != nil {
		postError(w, http.StatusInternalServerError)
		return
	}

	jwtToken, err := rest.TokenService.VerifyToken(td.Token, details.Hash)

	if err != nil {
		postBodyResponse(w, http.StatusOK, jsonResponse{"token": "invalid"})
		return
	}

	if jwtToken.Valid {
		postBodyResponse(w, http.StatusOK, jsonResponse{"token": "valid"})
		return
	}

	postBodyResponse(w, http.StatusOK, jsonResponse{"token": "invalid"})

}
