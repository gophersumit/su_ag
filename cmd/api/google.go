package api

import (
	"net/http"
	"strings"

	"gophersumit.com/sa-users/pkg/forms"
)

// GoogleSignUp using Google
func (rest *REST) GoogleSignUp(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		rest.googleSignUp(w, r)
		return

	default:
		postError(w, http.StatusMethodNotAllowed)
	}
}

// CheckEmail checks if email is alredy registed
func (rest *REST) CheckEmail(w http.ResponseWriter, r *http.Request) {

	email := strings.TrimPrefix(r.URL.Path, "/api/checkemail/")

	valid := forms.EmailRX.MatchString(email)

	if !valid {
		postError(w, http.StatusBadRequest)
		return
	}

	switch r.Method {
	case http.MethodGet:
		rest.checkEmail(w, r, email)
		return

	default:
		postError(w, http.StatusMethodNotAllowed)
	}
}

func (rest *REST) googleSignUp(w http.ResponseWriter, r *http.Request) {
	ge := new(GoogleEmail)
	err := bodyToGoogleEmail(w, r, ge)

	if err != nil {
		postError(w, http.StatusBadRequest)
		return
	}

	id, err := rest.Users.CreateGoogleUser(ge)

	if err != nil {
		postError(w, http.StatusInternalServerError)

		return
	}
	postBodyResponse(w, http.StatusCreated, jsonResponse{"id": id, "exists": true})

}

func (rest *REST) checkEmail(w http.ResponseWriter, r *http.Request, email string) {

	id, err := rest.Users.CheckEmail(email)

	if err != nil {
		postBodyResponse(w, http.StatusOK, jsonResponse{"id": 0, "exists": false})
		return
	}

	postBodyResponse(w, http.StatusOK, jsonResponse{"id": id, "exists": true})
	return
}
