package api

import (
	"errors"
	"net/http"

	"gophersumit.com/sa-users/pkg/models"
)

// Login to system
func (rest *REST) Login(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		rest.login(w, r)
		return

	default:
		postError(w, http.StatusMethodNotAllowed)
	}
}

func (rest *REST) login(w http.ResponseWriter, r *http.Request) {
	l := new(LoginUser)
	err := bodyToLogin(r, l)

	if err != nil {
		postError(w, http.StatusBadRequest)
		return
	}

	id, err := rest.Users.Authenticate(l.Email, l.Password)

	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			postError(w, http.StatusNotFound)
			return
		}
		postError(w, http.StatusUnauthorized)
		return
	}

	postBodyResponse(w, http.StatusOK, jsonResponse{"id": id})

}
