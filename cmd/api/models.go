package api

// Email is input accepted by send api
type Email struct {
	ToEmail   string `json:"to_email,omitempty"`
	ResetLink string `json:"reset_link,omitempty"`
	Subject   string `json:"subject,omitempty"`
}

type jsonResponse map[string]interface{}

//LoginUser is request to login to system
type LoginUser struct {
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

// RegisterUser is request for new user registration
type RegisterUser struct {
	Name     string `json:"name,omitempty"`
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

// UserEmail model
type UserEmail struct {
	Email string `json:"email,omitempty"`
}

//ChangePasswordRequest holds date to change password
type ChangePasswordRequest struct {
	Email       string `json:"email,omitempty"`
	NewPassword string `json:"new_password,omitempty"`
}

// TokenData represents token for email
type TokenData struct {
	Token string `json:"token,omitempty"`
	Email string `json:"email,omitempty"`
}

// GoogleEmail email address
type GoogleEmail struct {
	Email string `json:"email"`
}
