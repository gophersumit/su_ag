package web

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gophersumit.com/sa-users/pkg/forms"
)

// LoginUser to system
func (app *App) LoginUser(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		app.loginUserForm(w, r)
		return

	case http.MethodPost:
		app.loginUser(w, r)
		return
	default:
		SendError(w, http.StatusMethodNotAllowed)
	}
}

// LogoutUser from system
func (app *App) LogoutUser(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		app.logoutUser(w, r)
		return
	default:
		SendError(w, http.StatusMethodNotAllowed)
	}
}
func (app *App) loginUserForm(w http.ResponseWriter, r *http.Request) {

	flash := app.Session.PopString(r, "flash")
	app.render(w, r, "login.page.html", &templateData{
		Flash: flash,
		Form:  forms.New(nil),
	})
}
func (app *App) loginUser(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := forms.New(r.PostForm)
	lr := new(LoginResponse)
	login := &LoginRequest{
		Email:    form.Get("email"),
		Password: form.Get("password"),
	}
	jsonReq, err := json.Marshal(login)

	if err != nil {
		log.Fatal(err)
	}
	url := fmt.Sprintf("%s/%s", app.BaseURL, "api/login")

	response, err := app.makeRequest(w, r, url, http.MethodPost, bytes.NewBuffer(jsonReq))

	if err != nil {
		app.serverError(w, err)
		return
	}

	if response.StatusCode == http.StatusNotFound {
		app.Session.Put(r, "flash", "Email Address is wrong")
		http.Redirect(w, r, "/user/login", http.StatusSeeOther)
		return
	}

	if response.StatusCode == http.StatusUnauthorized {
		app.Session.Put(r, "flash", "Invalid Credentails")
		http.Redirect(w, r, "/user/login", http.StatusSeeOther)
		return
	}

	if response.StatusCode == http.StatusOK {
		defer response.Body.Close()
		json.NewDecoder(response.Body).Decode(&lr)
		app.Session.Put(r, "authenticatedUserID", lr.ID)
		app.Session.Put(r, "userEmail", login.Email)
		http.Redirect(w, r, "/user/profile", http.StatusSeeOther)
	}

}

func (app *App) logoutUser(w http.ResponseWriter, r *http.Request) {
	app.Session.Remove(r, "authenticatedUserID")
	app.Session.Put(r, "flash", "You've been logged out successfully!")

	http.Redirect(w, r, "/", 303)
}
