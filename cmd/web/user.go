package web

import (
	"net/http"
)

// Default application route
func (app *App) Default(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
	} else {
		http.Redirect(w, r, "/user/profile", http.StatusSeeOther)
	}
}
