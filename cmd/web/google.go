package web

import (
	"bytes"
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"gophersumit.com/sa-users/cmd/api"
)

// Scopes: OAuth 2.0 scopes provide a way to limit the amount of access that is granted to an access token.
var googleOauthConfig = &oauth2.Config{
	RedirectURL:  os.Getenv("REDIREST_URL"),
	ClientID:     os.Getenv("CLIENT_ID"),
	ClientSecret: os.Getenv("CLIENT_SECRET"),
	Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
	Endpoint:     google.Endpoint,
}

const oauthGoogleURLAPI = "https://www.googleapis.com/oauth2/v2/userinfo?access_token="

// UserInfo is returned by Google
type UserInfo struct {
	ID            string `json:"id"`
	Email         string `json:"email"`
	VerifiedEmail bool   `json:"verified_email"`
	Picture       string `json:"picture"`
}

// OAuthGoogleLogin initates login with google
func (app *App) OAuthGoogleLogin(w http.ResponseWriter, r *http.Request) {

	// Create oauthState cookie
	oauthState := generateStateOauthCookie(w)

	/*
		AuthCodeURL receive state that is a token to protect the user from CSRF attacks. You must always provide a non-empty string and
		validate that it matches the the state query parameter on your redirect callback.
	*/
	u := googleOauthConfig.AuthCodeURL(oauthState)
	http.Redirect(w, r, u, http.StatusTemporaryRedirect)
}

// OAuthGoogleCallback is registed at route called by google
func (app *App) OAuthGoogleCallback(w http.ResponseWriter, r *http.Request) {
	oauthState, _ := r.Cookie("oauthstate")

	if r.FormValue("state") != oauthState.Value {
		log.Println("invalid oauth google state")
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	data, err := getUserDataFromGoogle(r.FormValue("code"))
	if err != nil {
		log.Println(err.Error())
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	userData := &UserInfo{}

	err = json.Unmarshal(data, &userData)

	if err != nil {
		SendError(w, http.StatusUnauthorized)
	}

	app.successFullGoogleAuth(w, r, userData)
}

func generateStateOauthCookie(w http.ResponseWriter) string {
	var expiration = time.Now().Add(20 * time.Minute)

	b := make([]byte, 16)
	rand.Read(b)
	state := base64.URLEncoding.EncodeToString(b)
	cookie := http.Cookie{Name: "oauthstate", Value: state, Expires: expiration}
	http.SetCookie(w, &cookie)

	return state
}

func getUserDataFromGoogle(code string) ([]byte, error) {
	// Use code to get token and get user info from Google.

	token, err := googleOauthConfig.Exchange(context.Background(), code)
	if err != nil {
		return nil, fmt.Errorf("code exchange wrong: %s", err.Error())
	}
	response, err := http.Get(oauthGoogleURLAPI + token.AccessToken)
	if err != nil {
		return nil, fmt.Errorf("failed getting user info: %s", err.Error())
	}
	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, fmt.Errorf("failed read response: %s", err.Error())
	}
	return contents, nil
}

func (app *App) successFullGoogleAuth(w http.ResponseWriter, r *http.Request, userData *UserInfo) {

	url := fmt.Sprintf("%s/%s/%s", app.BaseURL, "api/checkemail", userData.Email)

	check := &CheckEmailResponse{}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	req.Header.Set("Content-Type", "application/json")

	response, err := app.makeRequest(w, r, url, http.MethodGet, nil)

	if response.StatusCode == http.StatusOK {

		defer response.Body.Close()
		err = json.NewDecoder(response.Body).Decode(&check)

		if err != nil {
			app.serverError(w, err)
		}

		if check.Exists {
			app.Session.Put(r, "authenticatedUserID", check.ID)
			app.Session.Put(r, "userEmail", userData.Email)
			http.Redirect(w, r, "/", http.StatusSeeOther)
			return
		}

		// Proceed to user registration
		registerUser := &api.GoogleEmail{
			Email: userData.Email,
		}

		jsonReq, err := json.Marshal(registerUser)

		if err != nil {
			log.Fatal(err)
		}
		url := fmt.Sprintf("%s/%s", app.BaseURL, "api/signup/google")

		response, err := app.makeRequest(w, r, url, http.MethodPost, bytes.NewBuffer(jsonReq))
		if err != nil {
			SendError(w, http.StatusInternalServerError)
		}
		if response.StatusCode == http.StatusCreated {
			defer response.Body.Close()
			err = json.NewDecoder(response.Body).Decode(&check)
			app.Session.Put(r, "authenticatedUserID", check.ID)
			app.Session.Put(r, "userEmail", userData.Email)
			http.Redirect(w, r, "/user/profile", http.StatusSeeOther)
		}

	}

}
