package web

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gophersumit.com/sa-users/pkg/forms"
)

// SignUpUser shows sign up page
func (app *App) SignUpUser(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		app.signupUser(w, r)
		return

	case http.MethodPost:
		app.signupUserForm(w, r)
		return
	default:
		SendError(w, http.StatusMethodNotAllowed)
	}
}

func (app *App) signupUserForm(w http.ResponseWriter, r *http.Request) {

	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := forms.New(r.PostForm)
	form.Required("name", "email", "password")
	form.Equal("password", "confirm")
	form.MaxLength("name", 255)
	form.MaxLength("email", 255)
	form.MatchesPattern("email", forms.EmailRX)
	form.MinLength("password", 10)

	if !form.Valid() {
		app.render(w, r, "signup.page.html", &templateData{Form: form})
		return
	}

	client := http.Client{}
	signUp := &SignUpRequest{
		Name:     form.Get("name"),
		Email:    form.Get("email"),
		Password: form.Get("password"),
	}
	jsonReq, err := json.Marshal(signUp)

	if err != nil {
		log.Fatal(err)
	}
	url := fmt.Sprintf("%s/%s", app.BaseURL, "api/signup/register")
	req, err := http.NewRequestWithContext(r.Context(), http.MethodPost, url, bytes.NewBuffer(jsonReq))
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)

	if resp.StatusCode == http.StatusForbidden {

		form.Errors.Add("email", "Email Address is already in use")
	}
	if !form.Valid() {
		app.render(w, r, "signup.page.html", &templateData{Form: form})
		return
	}

	if resp.StatusCode == http.StatusCreated {
		http.Redirect(w, r, "/user/login", http.StatusSeeOther)
	}
}
func (app *App) signupUser(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "signup.page.html", &templateData{
		Form: forms.New(nil),
	})
}
