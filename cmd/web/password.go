package web

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"gophersumit.com/sa-users/pkg/forms"
)

// ResetPassword of user
func (app *App) ResetPassword(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		app.resetPassword(w, r)
		return
	case http.MethodPost:
		app.resetPasswordForm(w, r)
		return
	default:
		SendError(w, http.StatusMethodNotAllowed)
	}
}

// ChangePassword of user
func (app *App) ChangePassword(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		app.changePassword(w, r)
		return

	case http.MethodPost:
		app.changePasswordForm(w, r)
		return

	default:
		SendError(w, http.StatusMethodNotAllowed)
	}
}

// UpdatePassword of user
func (app *App) UpdatePassword(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		app.updatePassword(w, r)
		return

	default:
		SendError(w, http.StatusMethodNotAllowed)
	}

}

func (app *App) changePassword(w http.ResponseWriter, r *http.Request) {
	querystrings := r.URL.Query()
	token := querystrings.Get("token")
	email := querystrings.Get("email")

	tr := &TokenValidationRequest{
		Token: token,
		Email: email,
	}
	client := http.Client{}

	validationResponse := &TokenValidationResponse{}
	jsonReq, err := json.Marshal(tr)

	if err != nil {
		SendError(w, http.StatusInternalServerError)
	}
	url := fmt.Sprintf("%s/%s", app.BaseURL, "api/validatetoken")
	req, err := http.NewRequestWithContext(r.Context(), http.MethodPost, url, bytes.NewBuffer(jsonReq))
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil || resp.StatusCode != http.StatusOK {
		app.serverError(w, err)
	}

	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK {
		json.NewDecoder(resp.Body).Decode(&validationResponse)
	}

	if validationResponse.Token == "valid" {
		app.render(w, r, "changepassword.page.html", &templateData{
			Form:  forms.New(nil),
			Email: email,
		})
		app.Session.Put(r, "userEmail", email)
	} else {
		app.Session.Put(r, "flash", "Password Reset Link is expired. Please request a new one.")

		http.Redirect(w, r, "/", 303)
	}

}

func (app *App) updatePassword(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "changepassword.page.html", &templateData{})
}

func (app *App) changePasswordForm(w http.ResponseWriter, r *http.Request) {
	email := app.Session.GetString(r, "userEmail")
	if email == "" {
		app.clientError(w, http.StatusBadRequest)
		return
	}
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := forms.New(r.PostForm)
	form.Required("password", "confirm")
	form.MinLength("password", 10)
	form.Equal("password", "confirm")
	if !form.Valid() {
		app.render(w, r, "changepassword.page.html", &templateData{Form: form})
		return
	}

	client := http.Client{}
	reset := &ChangePasswordRequest{
		Email:       email,
		NewPassword: form.Get("password"),
	}

	jsonReq, err := json.Marshal(reset)

	url := fmt.Sprintf("%s/%s", app.BaseURL, "api/changepassword")
	req, err := http.NewRequestWithContext(r.Context(), http.MethodPost, url, bytes.NewBuffer(jsonReq))
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		app.serverError(w, err)
	}
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK {
		app.Session.Put(r, "flash", "Password has been changed successfully")
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}

}
