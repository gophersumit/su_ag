package web

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"runtime/debug"
)

type contextKey string

// JsResponse for genreating JsonResponse
type JsResponse map[string]interface{}

const contextKeyIsAuthenticated = contextKey("isAuthenticated")

func (app *App) render(w http.ResponseWriter, r *http.Request, name string, td *templateData) {
	ts, ok := app.TemplateCache[name]
	if !ok {
		app.serverError(w, fmt.Errorf("The template %s does not exist", name))
		return
	}

	buf := new(bytes.Buffer)
	err := ts.Execute(buf, app.addDefaultData(td, r))
	if err != nil {
		app.serverError(w, err)
		return
	}

	buf.WriteTo(w)
}

func (app *App) serverError(w http.ResponseWriter, err error) {
	trace := fmt.Sprintf("%s\n%s", err.Error(), debug.Stack())
	app.errorLog.Output(2, trace)

	if app.debug {
		http.Error(w, trace, http.StatusInternalServerError)
		return
	}

	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}

func (app *App) addDefaultData(td *templateData, r *http.Request) *templateData {
	if td == nil {
		td = &templateData{
			Flash: "Hello",
		}
	}
	return td
}

func (app *App) clientError(w http.ResponseWriter, status int) {
	http.Error(w, http.StatusText(status), status)
}

func (app *App) makeRequest(w http.ResponseWriter, r *http.Request, url string, method string, body io.Reader) (*http.Response, error) {
	client := http.Client{}

	req, err := http.NewRequest(method, url, body)
	if err != nil {
		app.serverError(w, err)

	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)

	if err != nil {
		app.serverError(w, err)

	}

	if resp.StatusCode >= 400 && resp.StatusCode < 500 {
		app.clientError(w, resp.StatusCode)
	}

	if resp.StatusCode >= 500 {
		app.serverError(w, errors.New("Internal Server Error"))
	}

	return resp, err
}

// PostJSONResponse sends content as json object
func PostJSONResponse(w http.ResponseWriter, code int, content JsResponse) {
	if content != nil {
		js, err := json.Marshal(content)
		if err != nil {
			SendError(w, http.StatusInternalServerError)
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(code)
		w.Write([]byte(js))
	}
}

// SendError sends status code and error
func SendError(w http.ResponseWriter, code int) {
	http.Error(w, http.StatusText(code), code)
}

// IsAuthenticated checks if request is authenticated
func (app *App) IsAuthenticated(r *http.Request) bool {
	isAuthenticated, ok := r.Context().Value(contextKeyIsAuthenticated).(bool)
	if !ok {
		return false
	}
	return isAuthenticated
}
