package web

import "gophersumit.com/sa-users/pkg/models"

//UserProfileResponse response from user profile get request
type UserProfileResponse struct {
	UserDetails models.UserDetails `json:"userDetails"`
}

// SignUpRequest new user signup request
type SignUpRequest struct {
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

//LoginRequest request to login to system
type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// LoginResponse response after login
type LoginResponse struct {
	ID int `json:"id"`
}

// ResetRequest to reset user passowrd
type ResetRequest struct {
	Email string `json:"email,omitempty"`
}

// ResetResponse from reset request
type ResetResponse struct {
	Token string `json:"token"`
}

// SendEmailRequest request to send email for password reset link
type SendEmailRequest struct {
	ToEmail   string `json:"to_email"`
	ResetLink string `json:"reset_link"`
	Subject   string `json:"subject"`
}

// TokenValidationRequest validate user token
type TokenValidationRequest struct {
	Token string `json:"token"`
	Email string `json:"email"`
}

//TokenValidationResponse status of token
type TokenValidationResponse struct {
	Token string `json:"token"`
}

// UpdateUserProfileRequest updates user profile details
type UpdateUserProfileRequest struct {
	Email       string `json:"email"`
	FullName    string `json:"full_name"`
	UserAddress string `json:"user_address"`
	Telephone   string `json:"telephone"`
}

// ChangePasswordRequest to change user password
type ChangePasswordRequest struct {
	Email       string `json:"email"`
	NewPassword string `json:"new_password"`
}

// CheckEmailResponse response from check email
type CheckEmailResponse struct {
	ID     int  `json:"id"`
	Exists bool `json:"exists"`
}
