package web

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gophersumit.com/sa-users/pkg/forms"
)

func (app *App) resetPassword(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "reset.page.html", &templateData{
		Form: forms.New(nil),
	})
}

func (app *App) resetPasswordForm(w http.ResponseWriter, r *http.Request) {

	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := forms.New(r.PostForm)
	form.Required("email")
	form.MatchesPattern("email", forms.EmailRX)

	if !form.Valid() {
		app.render(w, r, "reset.page.html", &templateData{Form: form})
		return
	}
	rr := new(ResetResponse)
	client := http.Client{}
	reset := &ResetRequest{
		Email: form.Get("email"),
	}
	jsonReq, err := json.Marshal(reset)

	if err != nil {
		log.Fatal(err)
	}
	url := fmt.Sprintf("%s/%s", app.BaseURL, "api/token")
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(jsonReq))
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		app.serverError(w, err)
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusForbidden {
		// user is not registed system user

		form.Errors.Add("email", "email address is not registed")

		if !form.Valid() {
			app.render(w, r, "reset.page.html", &templateData{Form: form})
			return
		}
	}

	if resp.StatusCode == http.StatusOK {
		json.NewDecoder(resp.Body).Decode(&rr)
	}

	er := &SendEmailRequest{
		ToEmail:   reset.Email,
		ResetLink: rr.Token,
		Subject:   "Reset Your password",
	}

	jsonReq, err = json.Marshal(er)
	if err != nil {
		log.Fatal(err)
	}
	url = fmt.Sprintf("%s/%s", app.BaseURL, "api/send")
	req, err = http.NewRequestWithContext(r.Context(), http.MethodPost, url, bytes.NewBuffer(jsonReq))
	req.Header.Set("Content-Type", "application/json")

	resp, err = client.Do(req)
	if err != nil {
		app.serverError(w, err)
	}
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK {
		app.Session.Put(r, "flash", "Password reset link send to your email")
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}

}
