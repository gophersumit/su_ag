package web

import (
	"html/template"
	"log"

	"github.com/golangcollege/sessions"
	"gophersumit.com/sa-users/cmd/api"
)

// App represents a web application
type App struct {
	Session       *sessions.Session
	TemplateCache map[string]*template.Template
	errorLog      *log.Logger
	debug         bool
	infoLog       *log.Logger
	API           *api.REST
	BaseURL       string
}
