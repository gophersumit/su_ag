package web

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"

	"gophersumit.com/sa-users/pkg/forms"
	"gophersumit.com/sa-users/pkg/models"
)

// UpdateProfile updates user profile
func (app *App) UpdateProfile(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		app.editUserProfile(w, r)
		return
	case http.MethodPost:
		app.editUserProfileForm(w, r)
		return

	default:
		SendError(w, http.StatusMethodNotAllowed)
	}
}

// CreateProfile creates user profile
func (app *App) CreateProfile(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		app.createUserProfile(w, r)
		return
	case http.MethodPost:
		app.createUserProfileForm(w, r)
		return

	default:
		SendError(w, http.StatusMethodNotAllowed)
	}
}

// UserProfile routes
func (app *App) UserProfile(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		app.userProfileForm(w, r)
		return

	case http.MethodPost:
		app.userProfile(w, r)
		return
	default:
		SendError(w, http.StatusMethodNotAllowed)
	}

}

func (app *App) userProfileForm(w http.ResponseWriter, r *http.Request) {
	profile := new(UserProfileResponse)

	app.getProfile(w, r, profile)

	if profile.UserDetails.Email == "" {
		http.Redirect(w, r, "/user/createprofile", http.StatusSeeOther)
		return
	}
	app.render(w, r, "userprofile.page.html", &templateData{
		UserDetails: &profile.UserDetails,
	})
}

func (app *App) userProfile(w http.ResponseWriter, r *http.Request) {
}

func (app *App) editUserProfile(w http.ResponseWriter, r *http.Request) {

	profile := new(UserProfileResponse)
	app.getProfile(w, r, profile)

	if profile.UserDetails.Email == "" {
		http.Redirect(w, r, "/user/createprofile", http.StatusSeeOther)
		return
	}

	app.render(w, r, "editprofile.page.html", &templateData{
		UserDetails: &profile.UserDetails,
	})
}

func (app *App) editUserProfileForm(w http.ResponseWriter, r *http.Request) {

	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := forms.New(r.PostForm)

	client := http.Client{}

	updateProfile := &UpdateUserProfileRequest{
		Email:       form.Get("email"),
		FullName:    form.Get("fullname"),
		UserAddress: form.Get("address"),
		Telephone:   form.Get("telephone"),
	}

	jsonReq, err := json.Marshal(updateProfile)

	if err != nil {
		log.Fatal(err)
	}
	url := fmt.Sprintf("%s/%s%d", app.BaseURL, "api/user/profile/", app.Session.GetInt(r, "authenticatedUserID"))
	req, err := http.NewRequestWithContext(r.Context(), http.MethodPut, url, bytes.NewBuffer(jsonReq))
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)

	if resp.StatusCode == http.StatusConflict {
		form.Errors.Add("email", "Address is already in use")
		fmt.Println("Address is already in use")
		app.render(w, r, "editprofile.page.html", &templateData{UserDetails: &models.UserDetails{
			Email:       form.Get("email"),
			FullName:    form.Get("fullname"),
			UserAddress: form.Get("address"),
			Telephone:   form.Get("telephone"),
		}})
		return
	}

	if resp.StatusCode == http.StatusOK {
		http.Redirect(w, r, "/user/profile", http.StatusSeeOther)
	}

}

func (app *App) createUserProfile(w http.ResponseWriter, r *http.Request) {

	email := app.Session.GetString(r, "userEmail")

	form := forms.New(url.Values{
		"email": []string{email},
	})
	app.render(w, r, "createprofile.page.html", &templateData{
		Form: form,
	})
}

func (app *App) createUserProfileForm(w http.ResponseWriter, r *http.Request) {
	email := app.Session.GetString(r, "userEmail")

	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := forms.New(r.PostForm)

	client := http.Client{}

	createProfile := &UpdateUserProfileRequest{
		Email:       email,
		FullName:    form.Get("fullname"),
		UserAddress: form.Get("address"),
		Telephone:   form.Get("telephone"),
	}

	jsonReq, err := json.Marshal(createProfile)

	if err != nil {
		log.Fatal(err)
	}
	url := fmt.Sprintf("%s/%s", app.BaseURL, "api/user/profile")
	req, err := http.NewRequestWithContext(r.Context(), http.MethodPost, url, bytes.NewBuffer(jsonReq))
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)

	if resp.StatusCode == http.StatusCreated {
		http.Redirect(w, r, "/user/profile", http.StatusSeeOther)
	}
}

func (app *App) getProfile(w http.ResponseWriter, r *http.Request, profile *UserProfileResponse) {
	id := app.Session.GetInt(r, "authenticatedUserID")
	client := http.Client{}
	url := fmt.Sprintf("%s/%s/%d", app.BaseURL, "/api/user/profile", id)

	req, err := http.NewRequestWithContext(r.Context(), http.MethodGet, url, nil)
	if err != nil {
		log.Fatal(err)
	}

	resp, err := client.Do(req)
	if err != nil {
		app.serverError(w, err)
	}
	defer resp.Body.Close()

	json.NewDecoder(resp.Body).Decode(&profile)
}
