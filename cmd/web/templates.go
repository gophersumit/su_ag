package web

import (
	"html/template"
	"path/filepath"

	"gophersumit.com/sa-users/pkg/forms"
	"gophersumit.com/sa-users/pkg/models"
)

type templateData struct {
	CSRFToken       string
	Flash           string
	Form            *forms.Form
	IsAuthenticated bool
	User            *models.User
	UserDetails     *models.UserDetails
	ChangePassword  *models.ChangePassword
	Email           string
}

// NewTemplateCache creates a cache after parsing template
func NewTemplateCache(dir string) (map[string]*template.Template, error) {
	cache := map[string]*template.Template{}

	pages, err := filepath.Glob(filepath.Join(dir, "*.page.html"))
	if err != nil {
		return nil, err
	}

	for _, page := range pages {
		name := filepath.Base(page)

		ts, err := template.New(name).ParseFiles(page)
		if err != nil {
			return nil, err
		}

		ts, err = ts.ParseGlob(filepath.Join(dir, "*.layout.html"))
		if err != nil {
			return nil, err
		}

		ts, err = ts.ParseGlob(filepath.Join(dir, "*.partial.html"))
		if err != nil {
			return nil, err
		}

		cache[name] = ts
	}

	return cache, nil
}
