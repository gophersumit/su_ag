package token

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
)

//Service provides methods related to tokens
type Service struct {
}

// CreateToken creates a new jwt token valid for 15 minutes
func (t *Service) CreateToken(userid int, hash string) (string, error) {
	var err error
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["user_id"] = int64(userid)
	atClaims["exp"] = time.Now().Add(time.Minute * 15).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString([]byte(hash))
	if err != nil {
		return "", err
	}
	return token, nil
}

// VerifyToken that token is valid
func (t *Service) VerifyToken(tokenString string, hash string) (*jwt.Token, error) {

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(hash), nil
	})
	if err != nil {
		return nil, err
	}
	return token, nil
}

//ExtractUserID provides userId from Token
func (t *Service) ExtractUserID(token *jwt.Token) (int, error) {

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok {
		userID, err := strconv.ParseUint(fmt.Sprintf("%.f", claims["user_id"]), 10, 64)
		if err != nil {
			return 0, err
		}

		uID := int(userID)
		return uID, nil

	}

	return 0, errors.New("Invalid user")

}
