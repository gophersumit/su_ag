package email

import (
	"bytes"
	"fmt"
	"mime/quotedprintable"
	"net/smtp"

	"gophersumit.com/sa-users/pkg/models"
)

//GMail implements functionality to send email via gmail account
type GMail struct {
	Password                string
	Username                string
	Host                    string
	MimeVersion             string
	ContentType             string
	ContentTransferEncoding string
	ContentDisposition      string
	Port                    int
}

//SendEmail is used to send email
func (g *GMail) SendEmail(email *models.Email) error {
	auth := smtp.PlainAuth("", g.Username, g.Password, g.Host)

	header := make(map[string]string)
	header["From"] = g.Username
	header["To"] = email.ToEmail
	header["Subject"] = email.Subject
	header["MIME-Version"] = g.MimeVersion
	header["Content-Type"] = g.ContentType
	header["Content-Transfer-Encoding"] = g.ContentTransferEncoding
	header["Content-Disposition"] = g.ContentDisposition

	headerMessage := ""
	for key, value := range header {
		headerMessage += fmt.Sprintf("%s: %s\r\n", key, value)
	}

	body := email.Body
	var bodyMessage bytes.Buffer
	temp := quotedprintable.NewWriter(&bodyMessage)
	temp.Write([]byte(body))
	temp.Close()

	finalMessage := headerMessage + "\r\n" + bodyMessage.String()
	var host = fmt.Sprintf("%s:%d", g.Host, g.Port)
	return smtp.SendMail(host, auth, g.Username, []string{email.ToEmail}, []byte(finalMessage))

}

// NewGmail creates a new gmail client with defaults
func NewGmail(username string, password string) *GMail {
	g := new(GMail)
	g.Host = "smtp.gmail.com"
	g.Port = 587
	g.MimeVersion = "1.0"
	g.ContentType = fmt.Sprintf("%s; charset=\"utf-8\"", "text/html")
	g.ContentTransferEncoding = "quoted-printable"
	g.ContentDisposition = "inline"
	g.Username = username
	g.Password = password
	return g
}
