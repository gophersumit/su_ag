package models

import (
	"errors"
	"time"
)

// Errors for database
var (
	ErrNoRecord           = errors.New("models: no matching record found")
	ErrInvalidCredentials = errors.New("models: invalid credentials")
	ErrDuplicateEmail     = errors.New("models: duplicate email")
)

// User struct represents user record in database
type User struct {
	ID             int
	Name           string
	Email          string
	HashedPassword []byte
	Created        time.Time
	IsGoogleUser   bool
	Active         bool
}

// Email represents email to be sent
type Email struct {
	Subject string
	Body    string
	ToEmail string
}

// UserDetails represents user prifle
type UserDetails struct {
	Email       string `json:"email,omitempty"`
	FullName    string `json:"full_name,omitempty"`
	UserAddress string `json:"user_address,omitempty"`
	Telephone   string `json:"telephone,omitempty"`
}

//TokenDetails are required for generating token for user
type TokenDetails struct {
	UserID int    `json:"user_id,omitempty"`
	Hash   string `json:"hash,omitempty"`
}

//ChangePassword to change password
type ChangePassword struct {
	Email    string
	Password string
}
