package mysql

import (
	"database/sql"
	"errors"
	"strings"

	"github.com/go-sql-driver/mysql"
	"gophersumit.com/sa-users/pkg/models"
)

// UserDetailsModel reprents database connection to user details table
type UserDetailsModel struct {
	DB *sql.DB
}

// Get Returns a user from database
func (m *UserDetailsModel) Get(id int) (*models.UserDetails, error) {
	ud := &models.UserDetails{}

	stmt := `SELECT users.email, user_details.fullname, user_details.useraddress, user_details.telephone FROM 
	users LEFT JOIN user_details ON (users.id = user_details.userId ) WHERE users.id  = ?`
	err := m.DB.QueryRow(stmt, id).Scan(&ud.Email, &ud.FullName, &ud.UserAddress, &ud.Telephone)
	if err != nil {
		return nil, err
	}
	return ud, nil
}

// Update User Details
func (m *UserDetailsModel) Update(id int, userDetails *models.UserDetails) error {
	var email string

	stmt := `SELECT email FROM users WHERE users.id  = ?`
	err := m.DB.QueryRow(stmt, id).Scan(&email)
	if err != nil {
		return err
	}

	if email != userDetails.Email {
		// todo add transaction
		stmt = `UPDATE users SET email = ? WHERE id = ?`
		_, err = m.DB.Exec(stmt, string(userDetails.Email), id)

		if err != nil {
			var mySQLError *mysql.MySQLError
			if errors.As(err, &mySQLError) {
				if mySQLError.Number == 1062 && strings.Contains(mySQLError.Message, "users_uc_email") {
					return models.ErrDuplicateEmail
				}
			}
			return err
		}
	}

	stmt = `UPDATE user_details SET fullname = ? , useraddress = ?,
	telephone = ?  WHERE userId = ?`
	_, err = m.DB.Exec(stmt, string(userDetails.FullName),
		string(userDetails.UserAddress), string(userDetails.Telephone),
		id)
	return err
}

// Create User Details
func (m *UserDetailsModel) Create(userDetails *models.UserDetails) error {
	var id int

	stmt := `SELECT id FROM users WHERE users.email  = ?`
	err := m.DB.QueryRow(stmt, string(userDetails.Email)).Scan(&id)
	if err != nil {
		return err
	}
	if id == 0 {
		return err
	}

	stmt = `SELECT userId FROM user_details WHERE user_details.userId  = ?`
	rows, err := m.DB.Query(stmt, id)

	if err != nil {
		return err
	}

	if rows.Next() {
		return errors.New("User Details Already Exists")

	}
	if err != nil {
		return err
	}

	// todo add transaction
	stmt = `INSERT INTO user_details (fullname, useraddress, telephone, userId )
    VALUES(?, ?, ?, ?)`
	_, err = m.DB.Exec(stmt, userDetails.FullName, userDetails.UserAddress, userDetails.Telephone, id)
	return err

}
