package mysql

import (
	"database/sql"
	"errors"
	"strings"

	"github.com/go-sql-driver/mysql"
	"golang.org/x/crypto/bcrypt"
	"gophersumit.com/sa-users/cmd/api"
	"gophersumit.com/sa-users/pkg/models"
)

// UserModel comments
type UserModel struct {
	DB *sql.DB
}

// Insert creates a new user in database
func (m *UserModel) Insert(name, email, password string) error {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), 12)
	if err != nil {
		return err
	}

	stmt := `INSERT INTO users (name, email, hashed_password, created)
    VALUES(?, ?, ?, UTC_TIMESTAMP())`

	_, err = m.DB.Exec(stmt, name, email, string(hashedPassword))
	if err != nil {
		var mySQLError *mysql.MySQLError
		if errors.As(err, &mySQLError) {
			if mySQLError.Number == 1062 && strings.Contains(mySQLError.Message, "users_uc_email") {
				return models.ErrDuplicateEmail
			}
		}
		return err
	}

	return nil
}

// Authenticate validates username and password with database
func (m *UserModel) Authenticate(email, password string) (int, error) {

	var id int

	stmt := "SELECT id  from users WHERE email = ? AND isGoogleUser = FALSE"
	row := m.DB.QueryRow(stmt, email)
	err := row.Scan(&id)

	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return 0, models.ErrNoRecord
		}
		return 0, err

	}
	var hashedPassword []byte
	stmt = "SELECT id, hashed_password FROM users WHERE email = ?"
	row = m.DB.QueryRow(stmt, email)
	err = row.Scan(&id, &hashedPassword)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return 0, models.ErrInvalidCredentials
		}
		return 0, err
	}

	err = bcrypt.CompareHashAndPassword(hashedPassword, []byte(password))
	if err != nil {
		if errors.Is(err, bcrypt.ErrMismatchedHashAndPassword) {
			return 0, models.ErrInvalidCredentials
		} else {
			return 0, err
		}
	}

	return id, nil
}

// Get Returns a user from database
func (m *UserModel) Get(id int) (*models.User, error) {
	u := &models.User{}

	stmt := `SELECT id, name, email, created, active FROM users WHERE id = ?`
	err := m.DB.QueryRow(stmt, id).Scan(&u.ID, &u.Name, &u.Email, &u.Created, &u.Active)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, models.ErrNoRecord
		} else {
			return nil, err
		}
	}

	return u, nil
}

//ChangePassword is used to set new password
func (m *UserModel) ChangePassword(email string, newPassword string) error {

	var id int
	stmt := `SELECT id FROM users WHERE users.email  = ?`
	err := m.DB.QueryRow(stmt, string(email)).Scan(&id)
	if err != nil {
		return err
	}
	if id == 0 {
		return err
	}

	newHashedPassword, err := bcrypt.GenerateFromPassword([]byte(newPassword), 12)
	if err != nil {
		return err
	}

	stmt = "UPDATE users SET hashed_password = ? WHERE email = ?"
	_, err = m.DB.Exec(stmt, string(newHashedPassword), string(email))
	return err
}

//GetHash get stored hash of password of user
func (m *UserModel) GetHash(email string) (*models.TokenDetails, error) {
	td := &models.TokenDetails{}
	stmt := `SELECT id, hashed_password FROM users WHERE email = ? and isGoogleUser = ?`
	err := m.DB.QueryRow(stmt, string(email), false).Scan(&td.UserID, &td.Hash)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return td, models.ErrNoRecord
		}
		return td, err
	}

	return td, nil
}

// CheckEmail Checks if email is present
func (m *UserModel) CheckEmail(email string) (int, error) {
	id := 0
	stmt := `SELECT id FROM users WHERE email  = ?`
	err := m.DB.QueryRow(stmt, string(email)).Scan(&id)

	if err != nil {
		return id, err
	}

	return id, nil
}

// CreateGoogleUser creates a new google user
func (m *UserModel) CreateGoogleUser(ge *api.GoogleEmail) (int, error) {
	stmt := `INSERT INTO users ( email,isGoogleUser, created)
	VALUES(?, ?, UTC_TIMESTAMP())`

	_, err := m.DB.Exec(stmt, string(ge.Email), bool(true))
	if err != nil {
		var mySQLError *mysql.MySQLError
		if errors.As(err, &mySQLError) {
			if mySQLError.Number == 1062 && strings.Contains(mySQLError.Message, "users_uc_email") {
				return 0, models.ErrDuplicateEmail
			}
		}
		return 0, err
	}
	return m.CheckEmail(ge.Email)

}
