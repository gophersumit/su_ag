package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/golangcollege/sessions"
	uuid "github.com/satori/go.uuid"
	"gophersumit.com/sa-users/cmd/api"
	"gophersumit.com/sa-users/cmd/web"
	"gophersumit.com/sa-users/pkg/models/email"
	"gophersumit.com/sa-users/pkg/models/mysql"
	"gophersumit.com/sa-users/pkg/token"
)

type mainApplication struct {
	api api.REST
	web web.App
}

var (
	host     = os.Getenv("HOST_DB")
	database = os.Getenv("DATABASE")
	user     = os.Getenv("DB_USER")
	password = os.Getenv("DB_USER_PASSWORD")
	baseURL  = "https://sa-usermanagement.azurewebsites.net"
)

func main() {

	var connectionString = fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?allowNativePasswords=true&parseTime=true", user, password, host, database)
	secret := uuid.NewV4().String()
	db, err := sql.Open("mysql", connectionString)
	checkError(err)
	defer db.Close()

	err = db.Ping()
	checkError(err)
	fmt.Println("Successfully created connection to database.")

	session := sessions.New([]byte(secret))
	session.Lifetime = 12 * time.Hour

	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	webApplicationCache, err := web.NewTemplateCache("./templates/web/")
	if err != nil {
		errorLog.Fatal(err)
	}

	gmail := email.NewGmail("sumitusermanagement@gmail.com", "Hello@12345")

	ts := token.Service{}

	restAPI := api.REST{
		Users:        &mysql.UserModel{DB: db},
		UserDetails:  &mysql.UserDetailsModel{DB: db},
		Emailer:      gmail,
		TokenService: &ts,
		BaseURL:      baseURL,
	}

	webApp := web.App{
		TemplateCache: webApplicationCache,
		Session:       session,
		API:           &restAPI,
		BaseURL:       baseURL,
	}

	app := &mainApplication{
		web: webApp,
		api: restAPI,
	}

	server := http.Server{
		Addr:    ":8080",
		Handler: app.web.Session.Enable(app.routes()),
	}

	err = server.ListenAndServe()

	if err != nil {
		errorLog.Fatalln(err)
	}
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
