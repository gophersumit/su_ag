package main

import (
	"net/http"
)

func (app *mainApplication) routes() http.Handler {

	mux := http.NewServeMux()

	mux.Handle("/api/", app.registerAPIRoutes())
	mux.Handle("/", app.registerWebRoutes())

	fileServer := http.FileServer(http.Dir("./templates/static/"))
	mux.Handle("/static/", http.StripPrefix("/static", fileServer))
	return mux

}

func (app *mainApplication) registerAPIRoutes() http.Handler {
	mux := http.NewServeMux()
	mux.HandleFunc("/api/signup/register", app.api.RegisterUser)
	mux.HandleFunc("/api/signup/google", app.api.GoogleSignUp)
	mux.HandleFunc("/api/user/profile", app.api.UserProfile)
	mux.HandleFunc("/api/user/profile/", app.api.UserProfile)
	mux.HandleFunc("/api/login", app.api.Login)
	mux.HandleFunc("/api/changepassword", app.api.ChangePassword)
	mux.HandleFunc("/api/send", app.api.Send)

	mux.HandleFunc("/api/token", app.api.Token)
	mux.HandleFunc("/api/validatetoken", app.api.ValidateToken)

	mux.HandleFunc("/api/checkemail/", app.api.CheckEmail)

	return mux
}

func (app *mainApplication) registerWebRoutes() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/", app.web.Default)
	mux.HandleFunc("/user/signup", app.web.SignUpUser)
	mux.HandleFunc("/user/login", app.web.LoginUser)
	mux.HandleFunc("/user/logout", app.web.LogoutUser)
	mux.HandleFunc("/user/reset", app.web.ResetPassword)
	mux.HandleFunc("/user/changepassword", app.web.ChangePassword)
	mux.HandleFunc("/user/updatepassword", app.web.UpdatePassword)

	mux.Handle("/user/updateprofile", app.web.Authenticate(app.web.RequireAuthentication(http.HandlerFunc(app.web.UpdateProfile))))
	mux.Handle("/user/createprofile", app.web.Authenticate(app.web.RequireAuthentication(http.HandlerFunc(app.web.CreateProfile))))
	mux.Handle("/user/profile", app.web.Authenticate(app.web.RequireAuthentication(http.HandlerFunc(app.web.UserProfile))))
	//mux.HandleFunc("/user/profile", app.web.UserProfile)

	mux.HandleFunc("/auth/google/login", app.web.OAuthGoogleLogin)
	mux.HandleFunc("/auth/google/callback", app.web.OAuthGoogleCallback)
	return mux
}
